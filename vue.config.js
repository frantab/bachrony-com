module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/styles/_fonts.scss";
                      @import "@/styles/_functions.scss";
                      @import "@/styles/_variables.scss";
                      @import "@/styles/_base.scss";`,
      },
    },
  },
};
