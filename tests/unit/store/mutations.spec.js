import mutations from '@/store/modules/Portfolio/mutations';
import {
  SET_ABOUT,
  SET_JOBS,
  SET_CERTIFICATES,
  SET_EDUCATION,
  SET_SKILLS,
  SET_TOOLS,
  SET_SELECTED_SKILL,
} from '@/store/modules/Portfolio/constants';

let state = {};

describe('Testing Portfolio mutations.', () => {
  beforeEach(() => {
    state = {
      about: [],
      jobs: [],
      certificates: [],
      education: [],
      skills: [],
      selectedSkill: null,
      tools: [],
    };
  });

  it(`${SET_ABOUT} should set state about to ['new']`, () => {
    const newAbout = ['new'];

    mutations[SET_ABOUT](state, newAbout);
    expect(state.about).toBe(newAbout);
  });

  it(`${SET_JOBS} should set state jobs to ['new']`, () => {
    const newJobs = ['new'];

    mutations[SET_JOBS](state, newJobs);
    expect(state.jobs).toBe(newJobs);
  });

  it(`${SET_CERTIFICATES} should set state certificates to ['new']`, () => {
    const newCertificates = ['new'];

    mutations[SET_CERTIFICATES](state, newCertificates);
    expect(state.certificates).toBe(newCertificates);
  });

  it(`${SET_EDUCATION} should set state education to ['new']`, () => {
    const newEducation = ['new'];

    mutations[SET_EDUCATION](state, newEducation);
    expect(state.education).toBe(newEducation);
  });

  it(`${SET_SKILLS} should set state skills to ['new']`, () => {
    const newSkills = ['new'];

    mutations[SET_SKILLS](state, newSkills);
    expect(state.skills).toBe(newSkills);
  });

  it(`${SET_SELECTED_SKILL} should set state selectedSkill to ['new']`, () => {
    const newSelectedSkill = ['new'];

    mutations[SET_SELECTED_SKILL](state, newSelectedSkill);
    expect(state.selectedSkill).toBe(newSelectedSkill);
  });

  it(`${SET_TOOLS} should set state tools to ['new']`, () => {
    const newTools = ['new'];

    mutations[SET_TOOLS](state, newTools);
    expect(state.tools).toBe(newTools);
  });
});
