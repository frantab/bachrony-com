import { shallowMount } from '@vue/test-utils';
import Headline from '@/components/layout/Headline.vue';

describe('Testing Headline component.', () => {
  it('Headline should correctly load.', () => {
    const wrapper = shallowMount(Headline);

    expect(wrapper.find('.headline').exists()).toBeTruthy();
  });
});
