import { shallowMount } from '@vue/test-utils';
import Header from '@/components/layout/Header.vue';

describe('Testing Header component.', () => {
  it('Header should correctly load.', () => {
    const wrapper = shallowMount(Header);

    expect(wrapper.find('.header').exists()).toBeTruthy();
  });

  describe('Testing Header\'s methods.', () => {
    const openedNavbar = 'header__navbar--opened';
    const closedNavbar = 'header__navbar--closed';
    const viewport = 'viewport--height';

    beforeEach(() => {
      document.body.className = '';
    });

    it(`ToggleNavbar should add to el 'header__navbar' classes '${openedNavbar}' and to body '${viewport}'.`, () => {
      const { vm } = shallowMount(Header);
      const { header } = vm.$refs;

      vm.toggleNavbar();
      expect(header.classList.contains(openedNavbar)).toBeTruthy();
      expect(header.classList.contains(closedNavbar)).toBeFalsy();
      expect(document.body.classList.contains(viewport)).toBeTruthy();
    });

    it(`ToggleNavbar should add and remove from el 'header__navbar' classes '${openedNavbar}' and to body '${viewport}'.`, () => {
      const { vm } = shallowMount(Header);
      const { header } = vm.$refs;

      vm.toggleNavbar();
      vm.toggleNavbar();
      expect(header.classList.contains(openedNavbar)).toBeFalsy();
      expect(header.classList.contains(closedNavbar)).toBeTruthy();
      expect(document.body.classList.contains(viewport)).toBeFalsy();
    });

    it(`CloseNavbar should remove class '${openedNavbar}', '${viewport} and add '${closedNavbar}'.`, () => {
      const { vm } = shallowMount(Header);
      const { header } = vm.$refs;

      vm.closeNavbar();
      expect(header.classList.contains(openedNavbar)).toBeFalsy();
      expect(header.classList.contains(closedNavbar)).toBeTruthy();
      expect(document.body.classList.contains(viewport)).toBeFalsy();
    });

    it(`CloseNavbar should remove class '${openedNavbar}', '${viewport} and add '${closedNavbar}'.`, () => {
      const { vm } = shallowMount(Header);
      const { header } = vm.$refs;

      vm.toggleNavbar();
      vm.closeNavbar();
      expect(header.classList.contains(openedNavbar)).toBeFalsy();
      expect(header.classList.contains(closedNavbar)).toBeTruthy();
      expect(document.body.classList.contains(viewport)).toBeFalsy();
    });
  });
});
