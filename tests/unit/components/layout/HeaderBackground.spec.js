import { shallowMount } from '@vue/test-utils';
import HeaderBackground from '@/components/layout/HeaderBackground.vue';

describe('Testing HeaderBackground component.', () => {
  it('HeaderBackground should correctly load.', () => {
    const wrapper = shallowMount(HeaderBackground);

    expect(wrapper.find('.header-background').exists()).toBeTruthy();
  });
});
