import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import Career from '@/components/portfolio/Career.vue';
import * as constants from '@/store/modules/Portfolio/constants';

Vue.use(Vuex);
const actions = {
  [constants.FETCH_JOBS]: () => {},
};
const state = {
  about: [],
};
const store = new Vuex.Store({
  modules: {
    Portfolio: {
      namespaced: true,
      state,
      actions,
      mutations: {},
      getters: {},
    },
  },
});

describe('Testing Career component.', () => {
  it('Career should correctly load.', () => {
    const wrapper = shallowMount(Career, {
      store,
    });

    expect(wrapper.find('#career').exists()).toBeTruthy();
  });
});
