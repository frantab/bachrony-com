import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import About from '@/components/portfolio/About.vue';
import * as constants from '@/store/modules/Portfolio/constants';

Vue.use(Vuex);
const actions = {
  [constants.FETCH_ABOUT]: () => {},
};
const state = {
  about: [],
};
const store = new Vuex.Store({
  modules: {
    Portfolio: {
      namespaced: true,
      state,
      actions,
      mutations: {},
      getters: {},
    },
  },
});

describe('Testing About component.', () => {
  it('About should correctly load.', () => {
    const wrapper = shallowMount(About, {
      store,
    });

    expect(wrapper.find('#about').exists()).toBeTruthy();
  });
});
