import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import Certificates from '@/components/portfolio/Certificates.vue';
import * as constants from '@/store/modules/Portfolio/constants';

Vue.use(Vuex);
const actions = {
  [constants.FETCH_CERTIFICATES]: () => {},
};
const state = {
  about: [],
};
const store = new Vuex.Store({
  modules: {
    Portfolio: {
      namespaced: true,
      state,
      actions,
      mutations: {},
      getters: {},
    },
  },
});

describe('Testing Certificates component.', () => {
  it('Certificates should correctly load.', () => {
    const wrapper = shallowMount(Certificates, {
      store,
    });

    expect(wrapper.find('#certificates').exists()).toBeTruthy();
  });
});
