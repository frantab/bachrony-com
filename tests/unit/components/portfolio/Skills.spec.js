import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import Skills from '@/components/portfolio/Skills.vue';
import * as constants from '@/store/modules/Portfolio/constants';

Vue.use(Vuex);
const actions = {
  [constants.FETCH_SKILLS]: () => {},
  [constants.FETCH_TOOLS]: () => {},
};
const mutations = {
  [constants.SET_SELECTED_SKILL]: () => {},
};
const state = {
  about: [],
};
const store = new Vuex.Store({
  modules: {
    Portfolio: {
      namespaced: true,
      state,
      actions,
      mutations,
      getters: {},
    },
  },
});

describe('Testing Skills component.', () => {
  it('Skills should correctly load.', () => {
    const wrapper = shallowMount(Skills, {
      store,
    });

    expect(wrapper.find('#skills').exists()).toBeTruthy();
  });
});
