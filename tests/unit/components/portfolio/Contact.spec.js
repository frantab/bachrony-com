import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import Contact from '@/components/portfolio/Contact.vue';
import * as constants from '@/store/modules/Portfolio/constants';

Vue.use(Vuex);
const actions = {
  [constants.FETCH_CONTACT]: () => {},
};
const state = {
  about: [],
};
const store = new Vuex.Store({
  modules: {
    Portfolio: {
      namespaced: true,
      state,
      actions,
      mutations: {},
      getters: {},
    },
  },
});

describe('Testing Contact component.', () => {
  it('Contact should correctly load.', () => {
    const wrapper = shallowMount(Contact, {
      store,
    });

    expect(wrapper.find('#contact').exists()).toBeTruthy();
  });
});
