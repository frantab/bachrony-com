import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import Education from '@/components/portfolio/Education.vue';
import * as constants from '@/store/modules/Portfolio/constants';

Vue.use(Vuex);
const actions = {
  [constants.FETCH_EDUCATION]: () => {},
};
const state = {
  about: [],
};
const store = new Vuex.Store({
  modules: {
    Portfolio: {
      namespaced: true,
      state,
      actions,
      mutations: {},
      getters: {},
    },
  },
});

describe('Testing Education component.', () => {
  it('Education should correctly load.', () => {
    const wrapper = shallowMount(Education, {
      store,
    });

    expect(wrapper.find('#education').exists()).toBeTruthy();
  });
});
