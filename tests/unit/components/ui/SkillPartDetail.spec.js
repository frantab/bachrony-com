import { shallowMount } from '@vue/test-utils';
import SkillPartDetail from '@/components/ui/SkillPartDetail.vue';

let propsData = {};

describe('Testing SkillPartDetail component.', () => {
  beforeEach(() => {
    propsData = {
      name: 'Test name',
      level: 1,
    };
  });

  it('SkillPartDetail should correctly load.', () => {
    const wrapper = shallowMount(SkillPartDetail, {
      propsData,
    });

    expect(wrapper.find('.skill-part-detail').exists()).toBeTruthy();
  });
});
