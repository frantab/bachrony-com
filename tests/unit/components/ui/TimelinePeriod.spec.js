import { shallowMount } from '@vue/test-utils';
import TimelinePeriod from '@/components/ui/TimelinePeriod.vue';

let propsData = {};

describe('Testing TimelinePeriod component.', () => {
  beforeEach(() => {
    propsData = {
      title: 'Title',
      time: '11/2019',
      link: undefined,
      company: 'FRANTA',
      projects: [],
      collapsable: false,
      collapsed: false,
    };
  });

  it('TimelinePeriod should correctly load.', () => {
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period').exists()).toBeTruthy();
  });

  it('TimelinePeriod should mount without link title.', () => {
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('a.timeline-period__title--link').exists()).toBeFalsy();
  });

  it('TimelinePeriod should mount with link title.', () => {
    propsData.link = 'www.bachrony.com';
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('a.timeline-period__title--link').exists()).toBeTruthy();
  });

  it('TimelinePeriod should mount with company in title and without link.', () => {
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('a.timeline-period__title--link').exists()).toBeFalsy();
    expect(wrapper.find('.timeline-period__company').exists()).toBeTruthy();
  });

  it('TimelinePeriod should mount without company in title and with link.', () => {
    propsData.company = '';
    propsData.link = 'www.bachrony.com';
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('a.timeline-period__title--link').exists()).toBeTruthy();
    expect(wrapper.find('.timeline-period__company').exists()).toBeFalsy();
  });

  it('TimelinePeriod should mount with company in title and with link.', () => {
    propsData.link = 'www.bachrony.com';
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('a.timeline-period__title--link').exists()).toBeTruthy();
    expect(wrapper.find('.timeline-period__company').exists()).toBeTruthy();
  });

  it('TimelinePeriod should mount without company in title and without link.', () => {
    propsData.link = undefined;
    propsData.company = '';
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('a.timeline-period__title--link').exists()).toBeFalsy();
    expect(wrapper.find('.timeline-period__company').exists()).toBeFalsy();
  });

  it('TimelinePeriod should mount without collapsable time parts.', () => {
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period__dot--collapsable').exists()).toBeFalsy();
  });

  it('TimelinePeriod should mount with collapsable time parts.', () => {
    propsData.collapsable = true;
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period__dot--collapsable').exists()).toBeTruthy();
  });

  it('TimelinePeriod should mount with collapsed time part.', () => {
    propsData.collapsable = true;
    propsData.collapsed = true;
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period__arrow--right').exists()).toBeTruthy();
  });

  it('TimelinePeriod should mount with expanded time part.', () => {
    propsData.collapsable = true;
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period__arrow--down').exists()).toBeTruthy();
  });

  it('TimelinePeriod should mount with visible content because is expanded.', () => {
    propsData.collapsable = true;
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period__content').isVisible()).toBeTruthy();
  });

  it('TimelinePeriod should mount with hidden content because is collapsed.', () => {
    propsData.collapsable = true;
    propsData.collapsed = true;
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period__content').isVisible()).toBeFalsy();
  });

  it('TimelinePeriod should mount with hidden content because is collapsed.', () => {
    propsData.collapsable = true;
    propsData.collapsed = true;
    const wrapper = shallowMount(TimelinePeriod, {
      propsData,
    });

    expect(wrapper.find('.timeline-period__content').isVisible()).toBeFalsy();
  });

  describe('Testing TimelinePeriods methods.', () => {
    it('TimelinePeriods method togglePeriod should negate isCollapsed variable.', () => {
      const wrapper = shallowMount(TimelinePeriod, {
        propsData,
      });

      expect(wrapper.vm.isCollapsed).toBeFalsy();
      wrapper.vm.togglePeriod();
      expect(wrapper.vm.isCollapsed).toBeTruthy();
      wrapper.vm.togglePeriod();
      expect(wrapper.vm.isCollapsed).toBeFalsy();
    });
  });
});
