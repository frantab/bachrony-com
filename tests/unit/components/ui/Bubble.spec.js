import { shallowMount } from '@vue/test-utils';
import Bubble from '@/components/ui/Bubble.vue';

let propsData = {};

describe('Testing Bubble component.', () => {
  beforeEach(() => {
    propsData = {
      name: 'js',
      level: 1,
      skill: {},
      clickAble: true,
      onBubbleClick: () => {},
    };
  });

  it('Bubble should correctly load.', () => {
    const wrapper = shallowMount(Bubble, {
      propsData,
    });

    expect(wrapper.find('.bubble').exists()).toBeTruthy();
  });

  it('Bubble should load clickable bubble.', () => {
    const wrapper = shallowMount(Bubble, {
      propsData,
    });

    expect(wrapper.find('.bubble--clickable').exists()).toBeTruthy();
  });

  it('Bubble shouldn\'t load clickable bubble.', () => {
    propsData.clickAble = false;
    const wrapper = shallowMount(Bubble, {
      propsData,
    });

    expect(wrapper.find('.bubble--clickable').exists()).toBeFalsy();
  });

  it('Bubble should load bubble just with first bar.', () => {
    propsData.level = 0;
    const { vm } = shallowMount(Bubble, {
      propsData,
    });

    expect(vm.$refs.secondBar.style.cssText).toBe('');
  });

  it('Bubble should load bubble just with second bar with 45deg transformation.', () => {
    propsData.level = 50;
    const { vm } = shallowMount(Bubble, {
      propsData,
    });

    expect(vm.$refs.secondBar.style.cssText).toBe('transform: rotate(45deg);');
  });

  it('Bubble should load bubble just with second bar with -135deg transformation.', () => {
    propsData.level = 100;
    const { vm } = shallowMount(Bubble, {
      propsData,
    });

    expect(vm.$refs.secondBar.style.cssText).toBe('transform: rotate(-135deg);');
  });

  it('Bubble should load bubble just with first bar.', () => {
    propsData.level = 101;
    const { vm } = shallowMount(Bubble, {
      propsData,
    });

    expect(vm.$refs.secondBar.style.cssText).toBe('');
  });

  describe('Testing Bubble\'s computed variables.', () => {
    it('NameFontSize should be \'1.5em\'', () => {
      const { vm } = shallowMount(Bubble, {
        propsData,
      });

      expect(vm.nameFontSize).toBe('1.5em');
    });

    it('NameFontSize should be \'1em\'', () => {
      propsData.name = 'css';
      const { vm } = shallowMount(Bubble, {
        propsData,
      });

      expect(vm.nameFontSize).toBe('1em');
    });

    it('NameFontSize should be \'0.7em\'', () => {
      propsData.name = 'python';
      const { vm } = shallowMount(Bubble, {
        propsData,
      });

      expect(vm.nameFontSize).toBe('0.7em');
    });
  });
});
