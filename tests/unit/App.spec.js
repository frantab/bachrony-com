import { shallowMount } from '@vue/test-utils';
import App from '@/App.vue';
import router from '@/router';

describe('Testing App component.', () => {
  it('App should correctly load.', () => {
    const wrapper = shallowMount(App, {
      router,
    });

    expect(wrapper.find('#App').exists()).toBeTruthy();
  });
});
