import { shallowMount } from '@vue/test-utils';
import Portfolio from '@/views/Portfolio.vue';

describe('Testing Portfolio view.', () => {
  it('Portfolio should correctly load.', () => {
    const wrapper = shallowMount(Portfolio);

    expect(wrapper.find('.portfolio').exists()).toBeTruthy();
  });
});
