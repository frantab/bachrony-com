# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2019-12-08
### Added
- 2 params for TimeLinePeriods for collapsable timeline periods.
- Test coverage in readme.

### Changed
- TimeLinePeriods for Career changet to collapsable.

## [1.2.0] - 2019-11-21
### Added
- Firebase config for prod and dev in .env files.
- New gitlab-ci job for deploying to dev.

### Changed
- Updated readme.

## [1.1.1] - 2019-11-19
### Changed
- Firebase import.

## [1.1.0] - 2019-11-19
### Added
- Unit tests.

## [1.0.1] - 2019-11-19
### Changed
- Color and position of '& p' in refactored styles.

## [1.0.0] - 2019-11-19
### Added
- Support for unit tests in Jest.
- Airbnb eslint.
- Scss global files with variables etc.
- Gitlab-ci yml with lint and unit tests job.

### Changed
- Updated based on @vue/cli 4.0.5
- Refactored all componnets.

## [0.1.0] - 2019-05-28
### Added
- First version of portfolio (there wasn't changelog so this is just anchor)

[Unreleased]: https://gitlab.com/frantab/bachrony-com/compare/master...develop
[1.3.0]: https://gitlab.com/frantab/bachrony-com/compare/v1.2.0...v1.3.0
[1.2.0]: https://gitlab.com/frantab/bachrony-com/compare/v1.1.1...v1.2.0
[1.1.1]: https://gitlab.com/frantab/bachrony-com/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/frantab/bachrony-com/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/frantab/bachrony-com/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/frantab/bachrony-com/compare/v0.1.0...v1.0.0
[0.1.0]: https://gitlab.com/frantab/bachrony-com/tree/v0.1.0