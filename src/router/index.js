import Vue from 'vue';
import VueRouter from 'vue-router';
import Portfolio from '@/views/Portfolio.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Portfolio',
    component: Portfolio,
  },
  {
    path: '/#about',
    name: 'About',
    component: Portfolio,
  },
  {
    path: '/#career',
    name: 'Career',
    component: Portfolio,
  },
  {
    path: '/#education',
    name: 'Education',
    component: Portfolio,
  },
  {
    path: '/#skills',
    name: 'Skills',
    component: Portfolio,
  },
  {
    path: '/#certificates',
    name: 'Certificates',
    component: Portfolio,
  },
  {
    path: '/#contact',
    name: 'Contact',
    component: Portfolio,
  },
  {
    path: '/*',
    name: 'Default',
    component: Portfolio,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
