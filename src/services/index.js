import db from '../firebase/init';

export const fetchAbout = () => new Promise((resolve, reject) => {
  db.collection('about')
    .get()
    .then((about) => {
      about.docs.forEach((doc) => {
        resolve(doc.data().paragraphs);
      });
    })
    .catch(error => reject(error));
});

export const fetchJobs = () => new Promise((resolve, reject) => {
  db.collection('jobs')
    .orderBy('start', 'desc')
    .get().then((jobsQuery) => {
      const jobs = [];

      jobsQuery.docs.forEach((doc) => {
        const {
          title, company, projects, start, link,
        } = doc.data();
        const date = new Date(1000 * start.seconds);
        const month = date.getMonth() + 1;

        jobs.push({
          title,
          company,
          projects,
          link,
          time: `${(`0${month}`).slice(-2)}/${date.getFullYear()}`,
        });
      });
      resolve(jobs);
    })
    .catch(error => reject(error));
});

export const fetchCertificates = () => new Promise((resolve, reject) => {
  db.collection('certificates')
    .orderBy('time', 'desc')
    .get()
    .then((certificatesQuery) => {
      const certificates = [];

      certificatesQuery.docs.forEach((doc) => {
        const { title, link, time } = doc.data();
        const date = new Date(1000 * time.seconds);

        certificates.push({
          title,
          link,
          time: `${(`0${date.getMonth() + 1}`).slice(-2)}/${date.getFullYear()}`,
        });
      });
      resolve(certificates);
    })
    .catch(error => reject(error));
});

export const fetchEducation = () => new Promise((resolve, reject) => {
  db.collection('education')
    .orderBy('start', 'desc')
    .get()
    .then((educationQuery) => {
      const education = [];

      educationQuery.docs.forEach((doc) => {
        const {
          title, start, end, description, link,
        } = doc.data();
        const startDate = new Date(1000 * start.seconds);
        const endDate = new Date(1000 * end.seconds);

        education.push({
          title,
          description,
          link,
          time: `${startDate.getFullYear()}-${endDate.getYear() - 100}`,
        });
      });
      resolve(education);
    })
    .catch(error => reject(error));
});

export const fetchSkills = () => new Promise((resolve, reject) => {
  db.collection('skills')
    .orderBy('level', 'desc')
    .get()
    .then((skillsQuery) => {
      const skills = [];

      skillsQuery.docs.forEach((doc) => {
        const { name, level, parts } = doc.data();

        skills.push({ name, level, parts });
      });
      resolve(skills);
    })
    .catch(error => reject(error));
});

export const fetchTools = () => new Promise((resolve, reject) => {
  db.collection('tools')
    .orderBy('level', 'desc')
    .get()
    .then((toolsQuery) => {
      const tools = [];

      toolsQuery.docs.forEach((doc) => {
        const { name, level } = doc.data();

        tools.push({ name, level });
      });
      resolve(tools);
    })
    .catch(error => reject(error));
});
