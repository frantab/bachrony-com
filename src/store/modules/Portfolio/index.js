import actions from './actions';
import mutations from './mutations';
import getters from './getters';

export const defaultState = {
  about: [],
  jobs: [],
  certificates: [],
  education: [],
  skills: [],
  selectedSkill: null,
  tools: [],
};

export default {
  namespaced: true,
  state: { ...defaultState },
  actions,
  mutations,
  getters,
};
