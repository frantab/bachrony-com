import {
  fetchAbout,
  fetchJobs,
  fetchCertificates,
  fetchEducation,
  fetchSkills,
  fetchTools,
} from '../../../services';
import {
  FETCH_ABOUT,
  SET_ABOUT,
  FETCH_JOBS,
  SET_JOBS,
  FETCH_CERTIFICATES,
  SET_CERTIFICATES,
  FETCH_EDUCATION,
  SET_EDUCATION,
  FETCH_SKILLS,
  SET_SKILLS,
  FETCH_TOOLS,
  SET_TOOLS,
  SET_SELECTED_SKILL,
} from './constants';

export default {
  async [FETCH_ABOUT]({ commit }) {
    commit(SET_ABOUT, await fetchAbout());
  },
  async [FETCH_JOBS]({ commit }) {
    commit(SET_JOBS, await fetchJobs());
  },
  async [FETCH_CERTIFICATES]({ commit }) {
    commit(SET_CERTIFICATES, await fetchCertificates());
  },
  async [FETCH_EDUCATION]({ commit }) {
    commit(SET_EDUCATION, await fetchEducation());
  },
  async [FETCH_SKILLS]({ commit }) {
    const skills = await fetchSkills();

    commit(SET_SELECTED_SKILL, skills[0]);
    commit(SET_SKILLS, skills);
  },
  async [FETCH_TOOLS]({ commit }) {
    commit(SET_TOOLS, await fetchTools());
  },
};
