import {
  SET_ABOUT,
  SET_JOBS,
  SET_CERTIFICATES,
  SET_EDUCATION,
  SET_SKILLS,
  SET_TOOLS,
  SET_SELECTED_SKILL,
} from './constants';

export default {
  [SET_ABOUT](state, about) {
    state.about = about;
  },
  [SET_JOBS](state, jobs) {
    state.jobs = jobs;
  },
  [SET_CERTIFICATES](state, certificates) {
    state.certificates = certificates;
  },
  [SET_EDUCATION](state, education) {
    state.education = education;
  },
  [SET_SKILLS](state, skills) {
    state.skills = skills;
  },
  [SET_SELECTED_SKILL](state, skill) {
    state.selectedSkill = skill;
  },
  [SET_TOOLS](state, tools) {
    state.tools = tools;
  },
};
