import Vue from 'vue';
import Vuex from 'vuex';

// MODULES
import Portfolio from './modules/Portfolio/index';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Portfolio,
  },
});
