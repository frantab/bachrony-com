[![coverage report](https://gitlab.com/frantab/bachrony-com/badges/master/coverage.svg)](https://gitlab.com/frantab/bachrony-com/commits/master) [![pipeline status](https://gitlab.com/frantab/bachrony-com/badges/master/pipeline.svg)](https://gitlab.com/frantab/bachrony-com/commits/master)
# My presentation website and repo brachrony.com
At bachrony.com you can find my CV and also this repo showes my work with vuejs, js, node, gitlab (git), etc...

I created production and development enviroment. Everything what is on production (master branch) is on bachrony.com. Development enviroment is used for my experiments and new features before release. You can find this version of app on dev.bachrony.com (I know it's dev and it should't be public, but it's public for presentation reasons).

I would like to say that this is my presentation repo, but I'm not working on it constatly. My experiences are changing almost every day 🙂. For example there was an year pause in my portfolio work and when I saw code after that year, well, it was terrible 😀. I just want to say...please at first look how old is last commit 👍🏻.

If you have any question please contant me at bachrony.com or here on gitlab 🤙

### Innstalling dependecies
```
npm ci
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build-prod
```

### Compiles and minifies for development
```
npm run build-dev
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
